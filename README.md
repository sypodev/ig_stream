# Configure

1. Instale o PHP: (https://windows.php.net/download#php-7.2)
2. [Install Composer](https://getcomposer.org/download/)
3. Clone o Repositório ou Baixe o arquivo e descompate (https://github.com/davidazevedo/InstagramLive-PHP/archive/master.zip)
4. Execute ```composer require adrifkat/instagram-api react/child-process``` na pasta clonada
5. Edite o nome de usuário e a senha dentro de `config.php` com os detalhes do instagram
6. Execute o script `goLive.php`. (`php -f goLive.php`)
7. Copie seu Stream-URL e Stream-Key e cole-os no seu software de streaming. [Consulte OBS-Setup](https://github.com/davidazevedo/InstagramLive-PHP#obs-setup)